row	column	type	sample	rep	concentration	volume
A	1	Standard	Standard 1	1	3.36	5
B	1	Standard	Standard 2	1	0.336	5
C	1	Standard	Standard 3	1	0.0336	5
D	1	Standard	Standard 4	1	0.00336	5
E	1	Standard	Standard 5	1	0.000336	5
F	1	Standard	Standard 6	1	0.0000336	5
G	1	Standard	Standard 7	1	0.00000336	5
H	1	Standard	Standard 8	1	NTC	5
A	2	Standard	Standard 1	2	3.36	5
B	2	Standard	Standard 2	2	0.336	5
C	2	Standard	Standard 3	2	0.0336	5
D	2	Standard	Standard 4	2	0.00336	5
E	2	Standard	Standard 5	2	0.000336	5
F	2	Standard	Standard 6	2	0.0000336	5
G	2	Standard	Standard 7	2	0.00000336	5
H	2	Standard	Standard 8	2	NTC	5
A	3	Unknown	46.PE1C0340.m0.92	2	1.8	5
B	3	Unknown	126.PE1C0340.m1.94	2	1.9	5
C	3	Unknown	136.PE1C0340.m3.32	2	1.8	5
D	3	Unknown	110.PE1C0340.m5.91	2	2.6	5
E	3	Unknown	360.IN1C0246.m0.95	2	1.6	5
F	3	Unknown	154.IN1C0246.m1.97	2	1.3	5
G	3	Unknown	424.IN1C0246.m5.98	2	1.5	5
H	3	Unknown	1034.BG1C0476.m5.95	2	1.4	5
A	4	Unknown	859.IN1C0262.m5.91	2	1.8	5
B	4	Unknown	866.IN1C0274.m6.24	2	1.6	5
C	4	Unknown	938.IN1C0249.m5.95	2	1.6	5
D	4	Unknown	871.BG1C0482.m5.91	2	2.1	5
E	4	Unknown	1029.IN1C0290.m5.98	2	1.6	5
F	4	Unknown	954.PE1C0329.m5.91	2	1.8	5
G	4	Unknown	785.IN1C0280.m5.95	2	1.4	5
H	4	Unknown	988.PE1C0323.m6.01	2	1.8	5
A	5	Unknown	901.PE1C0307.m5.95	2	1.7	5
B	5	Unknown	895.PE1C0328.m5.98	2	1.9	5
C	5	Unknown	965.PE1C0347.m5.91	2	1.9	5
D	5	Unknown	2796.IN1C0249.m0.92	2	2.9	5
E	5	Unknown	2900.IN1C0249.m2.99	2	3.2	5
F	5	Unknown	2789.IN1C0262.m0.59	2	3.8	5
G	5	Unknown	2940.IN1C0262.m3.22	2	3.5	5
H	5	Unknown	2851.IN1C0274.m1.12	2	3.9	5
A	6	Unknown	2922.IN1C0274.m3.06	2	4.4	5
B	6	Unknown	2843.IN1C0280.m1.08	2	3.8	5
C	6	Unknown	2885.IN1C0280.m2.96	2	3.1	5
D	6	Unknown	2832.IN1C0290.m1.05	2	4.5	5
E	6	Unknown	2872.IN1C0290.m2.92	2	3.7	5
F	6	Unknown	2834.PE1C0307.m1.05	2	3.0	5
G	6	Unknown	2873.PE1C0307.m2.92	2	3.6	5
H	6	Unknown	2805.PE1C0323.m0.95	2	3.0	5
A	7	Unknown	2888.PE1C0323.m2.96	2	2.9	5
B	7	Unknown	2845.PE1C0328.m1.08	2	3.3	5
C	7	Unknown	2816.PE1C0329.m0.99	2	2.8	5
D	7	Unknown	2938.PE1C0329.m3.19	2	3.4	5
E	7	Unknown	2806.PE1C0331.m0.95	2	3.0	5
F	7	Unknown	2944.PE1C0331.m3.42	2	3.5	5
G	7	Unknown	2807.PE1C0336.m0.95	2	3.7	5
H	7	Unknown	2874.PE1C0336.m2.92	2	3.1	5
A	8	Unknown	2817.PE1C0343.m0.99	2	4.4	5
B	8	Unknown	2892.PE1C0343.m2.96	2	3.1	5
C	8	Unknown	2799.PE1C0347.m0.92	2	2.6	5
D	8	Unknown	2936.PE1C0347.m3.15	2	2.7	5
E	8	Unknown	2829.BG1C0467.m1.02	2	4.2	5
F	8	Unknown	2128.BG1C0467.m1.94	2	2.3	5
G	8	Unknown	2877.BG1C0467.m2.92	2	4.5	5
H	8	Unknown	2795.BG1C0476.m0.89	2	3.0	5
A	9	Unknown	2897.BG1C0476.m2.96	2	4.2	5
B	9	Unknown	2800.BG1C0482.m0.92	2	3.4	5
C	9	Unknown	2907.BG1C0482.m2.99	2	4.2	5
D	9	Unknown	2437.PE1C0336.m5.98	2	4.3	5
E	9	Unknown	2762.PE1C0331.m5.98	2	1.0	5
F	9	Unknown	2155.BG1C0476.m1.91	2	3.1	5
G	9	Unknown	2101.BG1C0482.m2	2	4.4	5
H	9	Unknown	2147.IN1C0249.m1.94	2	3.0	5
A	10	Unknown	2112.IN1C0262.m1.94	2	3.6	5
B	10	Unknown	2148.IN1C0274.m2.33	2	3.6	5
C	10	Unknown	2149.IN1C0290.m1.91	2	3.5	5
D	10	Unknown	2114.PE1C0307.m2.04	2	3.4	5
E	10	Unknown	2115.PE1C0323.m2	2	2.8	5
F	10	Unknown	2142.PE1C0328.m1.97	2	2.8	5
G	10	Unknown	2160.PE1C0331.m1.91	2	3.2	5
H	10	Unknown	2106.PE1C0336.m1.91	2	2.4	5
A	11	Unknown	2143.PE1C0343.m2	2	4.2	5
B	11	Unknown	2161.PE1C0347.m2	2	2.4	5
C	11	Unknown				
D	11	Unknown				
E	11	Unknown				
F	11	Unknown				
G	11	Unknown				
H	11	Unknown				
A	12	Unknown				
B	12	Unknown				
C	12	Unknown				
D	12	Unknown				
E	12	Unknown				
F	12	Unknown				
G	12	Unknown				
H	12	Unknown				